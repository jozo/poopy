import sys
import time

from django.conf import settings
from django.http import StreamingHttpResponse
from django.urls import path
from django.core.management import execute_from_command_line

settings.configure(
    DEBUG=True,
    ROOT_URLCONF=sys.modules[__name__],
)


def poop():
    yield "<html><head><title>Poopy time</title></head><body><h1>"
    while True:
        yield "💩".encode()
        time.sleep(0.2)


def index(request):
    return StreamingHttpResponse(poop())


urlpatterns = [path('', index)]


if __name__ == '__main__':
    execute_from_command_line(sys.argv)
