# poopy - stream of 💩

![Screenshot](screenshot.gif)

**Demo:** https://poopy.fly.dev/

## Install
```
pip install django
```

## Run
```
python main.py runserver
```

Open [localhost:8000](http://localhost:8000)

## Docker

```
docker build -t my-python-app .
docker run -it --rm --name my-running-app -p 8000:8000 my-python-app
```