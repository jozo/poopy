FROM python:3.10.6-slim-bullseye

WORKDIR /app

RUN pip install django

COPY . .

CMD [ "python", "main.py", "runserver", "0:8000" ]
